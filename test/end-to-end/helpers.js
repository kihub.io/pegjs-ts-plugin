
const ts = require('typescript');
const fs = require('fs');
const pegjs = require('../../src/pegjs');
const pegjsTSPlugin = require('../../src/pegjs-ts-plugin');

function compileAndRequire(grammar, consumer, pegjsExtraOptions) {

    // Compile the grammar
    const pegjsOptions = Object.assign({
        format: "es",
        plugins: [ pegjsTSPlugin ],
        output: 'source'
    }, ( pegjsExtraOptions || {} ));
    const parserText = pegjs.generate( grammar, pegjsOptions );

    // TS Compile
    const TSFiles = {
        'lib.d.ts': { text: fs.readFileSync(require.resolve('typescript/lib/lib.d.ts')).toString() },
        '/parser.ts': { text: parserText }
    };
    if (consumer)
        TSFiles['/consumer.ts'] = { text: consumer };

    Object.keys(TSFiles).forEach( filename => {
        const entry = TSFiles[filename];
        entry.ast = ts.createSourceFile(filename, entry.text, ts.ScriptTarget.ES2016);
    });

    const host = {
        fileExists: filePath => !!TSFiles[filePath],
        directoryExists: dirPath => dirPath === '/',
        getCurrentDirectory: () => "/",
        getDirectories: () => [],
        getCanonicalFileName: filename => filename,
        getNewLine: () => "\n",
        getDefaultLibFileName: () => "lib",
        getSourceFile: filePath => TSFiles[filePath]? TSFiles[filePath].ast : undefined,
        readFile: filePath => TSFiles[filePath]? TSFiles[filePath].text : undefined,
        useCaseSensitiveFileNames: () => true,
        writeFile: () => {}
    };

    const rootFile = consumer? "/consumer.ts" : "/parser.ts";
    const program = ts.createProgram([ rootFile ], { strict: true }, host);

    const d = ts.getPreEmitDiagnostics(program);
    return d;
}



module.exports = function ( chai, utils ) {

    chai.Assertion.addMethod('compileWithoutErrors', function( pegOptions ) {

        const diag = compileAndRequire( this._obj, null, pegOptions );
        const nErrors = diag.length;
   
        this.assert(nErrors === 0,
            `expected generated parser to compile without errors, but got ${nErrors} errors`,
            "expected generated parser to fail compilation but it succeedded",
            0,
            nErrors
        );
    });

    chai.Assertion.addMethod('compileWithError', function( errorMessage, pegOptions ) {

        const diag = compileAndRequire( this._obj, null, pegOptions );
        const nErrors = diag.length;

        this.assert((nErrors === 1) && (diag[0].messageText === errorMessage),
            `expected generated parser to compile with a single error, but got ${nErrors} errors`,
            "expected generated parser to not have a single error",
            errorMessage,
            `${nErrors} errors`
        );
  
    });

    chai.Assertion.addMethod('indirectlyCompileWithoutErrors', function( consumer, pegOptions ) {

        const diag = compileAndRequire( this._obj, consumer, pegOptions );
        const nErrors = diag.length;
   
        this.assert(nErrors === 0,
            `expected generated parser to compile without errors, but got ${nErrors} errors`,
            "expected generated parser to fail compilation but it succeedded",
            0,
            nErrors
        );
    });

    chai.Assertion.addMethod('indirectlyCompileWithError', function( consumer, errorMessage, pegOptions ) {

        const diag = compileAndRequire( this._obj, consumer, pegOptions );
        const nErrors = diag.length;

        this.assert((nErrors === 1) && (diag[0].messageText === errorMessage),
            `expected generated parser to compile with a single error, but got ${nErrors} errors`,
            "expected generated parser to not have a single error",
            errorMessage,
            `${nErrors} errors`
        );
  
    });
}

