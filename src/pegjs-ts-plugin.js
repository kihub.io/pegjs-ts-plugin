exports.use = function (config, options) {

  config.parser = require("./parser/parser")

  // Add the infer-types before the first pass
  config.passes.generate = [ require("./passes/infer-types") ].concat(config.passes.generate)

  // Replace the "generate bytecode" step with the TS step
  const genBytecodeIndex = config.passes.generate.findIndex(item => item.name === "generateBytecode");
  config.passes.generate[genBytecodeIndex] = require('./passes/generate-bytecode-ts');

  // Replace the "generate js" step with the TS step
  const genJSIndex = config.passes.generate.findIndex(item => item.name === "generateJS");
  config.passes.generate[genJSIndex] = require('./passes/generate-ts');
  
  if (!options.tspegjs) {
    options.tspegjs = {};
  }
  if (options.tspegjs.noTslint === undefined) {
    options.tspegjs.noTslint = false;
  }
  
};
