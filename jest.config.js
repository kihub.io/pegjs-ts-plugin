module.exports = {
    roots: [
        'test/unit',
        'test/end-to-end'
    ],
    reporters: [ "default", "jest-junit" ]
};
