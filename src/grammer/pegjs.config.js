"use strict";

module.exports = {

    input: "src/grammer/parser.pegjs",
    output: "src/parser/parser.js",

    header: "/* eslint-disable */",

    dependencies: {

        ast: "pegjs/lib/ast",
        util: "pegjs/lib/util"

    },

    features: {

        offset: false,
        range: false,
        expected: false,

    },

};
