'use strict'

const chai = require('chai');
const helpers = require('./helpers');

chai.use( helpers );
const { expect } = chai;


describe('Tutorial Examples', function() {

    it('Simple rule type assignment', function() {
        const grammar = 'start = digits:[0-9]+ <number>{ return parseInt(digits.join()); }';
        const consumer1 = `
        import { parse } from "./parser";
        const q: number = parse('123');        
        `

        expect( grammar ).to.indirectlyCompileWithoutErrors( consumer1 )


        const consumer2 = `
            import { parse } from "./parser";
            const q: string = parse('123');        
            `
        expect( grammar ).to.indirectlyCompileWithError( consumer2, "Type 'number' is not assignable to type 'string'." );
    });

    it('Code block argument types', function() {

        const grammar = `
        move = dir:direction " " steps:number <number>{
            const sign = dir === "forward"? 1 : -1;
            return sign*steps;
        }
        direction = 'forward' / 'backward';
        number = digits:[0-9]+ <number>{ return parseInt(digits.join()); }
        `

        const consumer = `
        import { parse } from './parser';
        //parse('123')
        `

        expect( grammar ).to.indirectlyCompileWithoutErrors( consumer );
    });

    it('Multiple start rules', function() {
        const grammar = `
        as_number = digits:[0-9]+ <number>{ return parseInt(digits.join()); }
        as_string = digits:[0-9]+ <string>{ return digits.join() }
        `;

        const consumer = `
        import { parse, parseAsNumber, parseAsString } from './parser'

        const p1 = parse('123', { startRule: 'as_number' }); // p1 type is: number|string
        const p1num: number = <number>p1;
        const p1str: string = <string>p1;
        const p2: number = parseAsNumber('123');  // p2 type is: number
        const p3: string = parseAsString('123');  // p3 type is: string
        `;

        expect( grammar ).to.indirectlyCompileWithoutErrors( consumer, { allowedStartRules: [ 'as_number', 'as_string' ]} );

    })
});
