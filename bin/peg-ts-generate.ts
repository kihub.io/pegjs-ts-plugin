
import { readFileSync, writeFileSync } from 'fs';
import { program } from 'commander';
import { generate, GenerateOptions } from '../src/peg-ts';

function parseCommaSeparated(value: string) {
    return value.split(' ');
}

program
    .option('-a, --allowed-start-rules <rules>', "Comma separated list of allowed start rules", parseCommaSeparated)
    .option('--cache', 'Make generated parser cache results', false)
    .option('-o, --output <filename>', 'Output file (stdout if not specified)')
    .option('-O, --optimize <goal>', 'Select optimization for speed or size (default: speed)', 'speed')
    .option('--trace', 'Enable tracing in generated parser', false)
    // TODO: Read from package.json
    .version('1.0.0', '-v, --version', 'Print version information and exit')
    .arguments('<input_file>')
    .parse(process.argv);

// Check that we got exactly one input file
if (program.args.length !== 1) {
    console.log(program.helpInformation());
    process.exit(1);
}
const inputFilename = program.args[0];

const generateOptions: GenerateOptions = {
    trace: program.trace,
    cache: program.cache
}

// Construct generate options
if (program.allowedStartRules)
    generateOptions.allowedStartRules = program.allowedStartRules

if (program.optimize === 'speed')
    generateOptions.optimize = 'speed';
else if (program.optimize === 'size')
    generateOptions.optimize = 'size';
else {
    console.error('Valid optimization goals are "size" or "speed"');
    process.exit(1);
}

try {
    const inputData = readFileSync(inputFilename, 'utf-8');
    const parser = generate(inputData, generateOptions);

    if (program.output)
        writeFileSync(program.output, parser);
    else
        process.stdout.write(parser);
}
catch(e) {
    console.error(e.message);
}
