import * as pegjs from './pegjs';
import * as pegjsTsPlugin from './pegjs-ts-plugin';

export interface GenerateOptions {
    allowedStartRules?: string[],
    cache?: boolean,
    optimize?: 'size' | 'speed',
    trace?: boolean
};

export function generate(grammar: string, options: GenerateOptions) {
    return(pegjs.generate(grammar, { 
        ...options, 
        format: 'es', 
        output: 'source',
        //allowedStartRules: options.allowedStartRules || [], 
        plugins: [ pegjsTsPlugin ] 
    }));
}
