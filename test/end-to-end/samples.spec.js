const chai = require('chai');
const pegjs = require('../../src/pegjs');
const tspegjs = require('../../src/pegjs-ts-plugin');
const path = require('path');
const fs = require('fs');
const helpers = require('./helpers');

chai.use( helpers );
const { expect } = chai;


describe('Samples', function() {

    const samples = [ 'arithmetics', /*'css',*/ 'javascript', 'json', /*'st'*/ ];

    for (const optimize of [ 'speed', 'size' ]) {
        for (const sample of samples) {
            it(`Complete sample: ${sample} optimization for ${optimize}`, function() {

                const sampleFilename = path.resolve(__dirname, '../../examples', `${sample}.pegjs`)
                const grammar = fs.readFileSync(sampleFilename).toString();

                expect( grammar ).to.compileWithoutErrors();
            });
        }
}
});
